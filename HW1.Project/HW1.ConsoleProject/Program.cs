﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Linq;
using HW1.BLL.Services;

namespace HW1.ConsoleProject
{
    class Program
    {
        static void Main(string[] args)
        {
            WebService service = new WebService();
            bool flag = true;
            using (HttpClient client = new HttpClient())
            {
                while (flag)
                {
                    Console.WriteLine("*********************************************************************************************************");
                    Console.WriteLine("\tWelcome to my console :)\n\tThere is list of all tasks:\n\n");
                    Console.WriteLine("1. Ammount of tasks of concrete user`s project.\n" +
                        "2. List of concrete user`s tasks with length < 45 symbols.\n" +
                        "3. List of concrete user`s tasks that were completed in 2021.\n" +
                        "4. List of teams with 10 and more y.o. users, sorted by time of registration discending and grouped by teams.\n" +
                        "5. List of users sorted by first name wits tasks, sorted by length of name discending.\n" +
                        "6. Special struct#1.\n" +
                        "7. Special struct#2.\n" +
                        "0. Exit\n" +
                        "*********************************************************************************************************\n\n");
                    Console.Write("\tWrite the number of task: ");
                    int answer = Int32.Parse(Console.ReadLine());
                    switch (answer)
                    {
                        case 1:
                            Console.WriteLine("\n\n\t1. Ammount of tasks of concrete user`s project.\n\n");
                            Console.Write("Write id of user: ");
                            int userIdTask1 = Int32.Parse(Console.ReadLine());
                            Console.WriteLine();
                            try
                            {
                                var task1 = service.GetAmmountOfTasksOfConcreteUser(userIdTask1);
                                foreach (var item in task1)
                                {
                                    Console.WriteLine("Project name: " + item.Key.Name + ", ammount of tasks: " + item.Value.ToString());
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            Console.WriteLine();
                            break;
                        default:
                            break;
                        case 2:
                            Console.WriteLine("\n\n\t2. List of concrete user`s tasks with length < 45 symbols.\n\n");
                            Console.Write("Write id of user: ");
                            int userIdTask2 = Int32.Parse(Console.ReadLine());
                            Console.WriteLine();
                            var task2 = service.GetAllTasksOfConcreteUser(userIdTask2);
                            try
                            {
                                foreach (var item in task2)
                                {
                                    Console.WriteLine("Task name: " + item.Name + " (" + item.Description + ")");
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            Console.WriteLine();
                            break;
                        case 3:
                            Console.WriteLine("\n\n\t3. List of concrete user`s tasks that were completed in 2021.\n\n");
                            Console.Write("Write id of user: ");
                            int userIdTask3 = Int32.Parse(Console.ReadLine());
                            Console.WriteLine();
                            try
                            {
                                var task3 = service.GetFinishedTasksOfConcreteUser(userIdTask3);
                                foreach (var item in task3)
                                {
                                    Console.WriteLine("Id: " + item.Key.ToString() + ", name: " + item.Value);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            Console.WriteLine();
                            break;
                        case 4:
                            Console.WriteLine("\n\n\t4. List of teams with 10 and more y.o. users, sorted by time of registration discending and grouped by teams.\n\n");
                            var task4 = service.GetTeamsWithUsersOlderThan10();
                            foreach (var item in task4)
                            {
                                Console.WriteLine("\tTeam id: " + item.Id.ToString() + ", team name: " + item.TeamName + ", users: ");
                                foreach (var user in item.Users)
                                {
                                    Console.Write(user.LastName + " " + user.FirstName + ", ");
                                }
                                Console.WriteLine("\n");
                            }
                            Console.WriteLine();
                            break;
                        case 5:
                            Console.WriteLine("\n\n\t5. List of users sorted by first name wits tasks, sorted by length of name discending.\n\n");
                            var task5 = service.GetSortedListOfUsers();
                            foreach (var item in task5)
                            {
                                Console.WriteLine("\tUser name: " + item.Key.FirstName + " " + item.Key.LastName + ", tasks: ");
                                foreach (var task in item.Value)
                                {
                                    Console.Write(task.Name + ", ");
                                }
                                Console.WriteLine("\n");
                            }
                            Console.WriteLine();
                            break;
                        case 6:
                            Console.WriteLine("\n\n\t6. Special struct#1.\n\n");
                            Console.Write("Write id of user: ");
                            int userIdTask6 = Int32.Parse(Console.ReadLine());
                            Console.WriteLine();
                            try
                            {
                                var task6 = service.GetUserStruct(userIdTask6);
                                Console.WriteLine("UserId: " + task6.User.Id + ", user name: " + task6.User.FirstName + " " + task6.User.LastName + ".");
                                Console.WriteLine("Last user`s project: " + task6.LastProject.Name + " (" + task6.LastProject.Description + ", " + task6.LastProject.CreatedAt.ToString() + ").");
                                Console.WriteLine("Ammount of tasks of last user`s project: " + task6.AmmountOfTaskInLastProject.ToString() + ".");
                                Console.WriteLine("Ammount of not finished or canceled tasks: " + task6.AmmoutOfNotFinishedOrClosedTasks.ToString());
                                Console.WriteLine("The longest user`s task: " + task6.TheLongestTask.Name + " (" + task6.TheLongestTask.Description + ").\n");
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            Console.WriteLine();
                            break;
                        case 7:
                            Console.WriteLine("\n\n\t7. Special struct#2.\n\n");
                            var task7 = service.GetProjectStruct();
                            foreach (var project in task7)
                            {
                                Console.WriteLine("\tProject: " + project.Project.Id.ToString() + ". " + project.Project.Name + " (" + project.Project.Description + ").");
                                if (project.theLongestTask != null)
                                {
                                    Console.WriteLine("The longest task: " + project.theLongestTask.Name + " (" + project.theLongestTask.Description + ").");
                                    Console.WriteLine("The shortest task: " + project.theShortestTask.Name + " (" + project.theShortestTask.Description + ").");
                                }
                                else
                                {
                                    Console.WriteLine("This project doesn`t contain any task");
                                }
                                if (project.ammountOfUsers != 0)
                                {
                                    Console.WriteLine("The ammount of users in project`s team: " + project.ammountOfUsers.ToString());
                                }
                                else
                                {
                                    Console.WriteLine("Description of the project <= 20 symbols and ammount of project`s task >= 3.");
                                }
                                Console.WriteLine("\n");
                            }
                            Console.WriteLine();
                            break;
                        case 0:
                            flag = false;
                            break;
                    }
                }
            }
        }
    }
}
