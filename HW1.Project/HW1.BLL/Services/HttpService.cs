﻿using HW1.BLL.ApiResultModels;
using HW1.BLL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HW1.BLL.Services
{
    public class HttpService
    {
        private List<Project> projects = new List<Project>();
        private Project project { get; set; }
        public List<Project> GetRequest ()
        {
            using (HttpClient client = new HttpClient())
            {
                List<ProjectModel> allProjects = GetAllProjects(client);
                List<TaskModel> allTasks = GetAllTasks(client);
                List<TeamModel> allTeams = GetAllTeams(client);
                List<UserModel> allUsers = GetAllUsers(client);
                var foundProjects = from project in allProjects
                                    join team in allTeams on project.TeamId equals team.Id
                                    join author in allUsers on project.AuthorId equals author.Id
                                    join task in allTasks on project.Id equals task.ProjectId into tasks
                                    select new Project
                                    {
                                        Id = project.Id,
                                        Name = project.Name,
                                        Description = project.Description,
                                        CreatedAt = project.CreatedAt,
                                        DeadLine = project.DeadLine,
                                        Team = new Team
                                        {
                                            Id = team.Id,
                                            Name = team.Name,
                                            CreatedAt = team.CreatedAt,
                                            Users = (
                                                from user in allUsers
                                                where user.TeamId == team.Id
                                                select new User
                                                {
                                                    Id = user.Id,
                                                    FirstName = user.FirstName,
                                                    Birthday = user.Birthday,
                                                    Email = user.Email,
                                                    LastName = user.LastName,
                                                    RegisteredAt = user.RegisteredAt
                                                }
                                            ).ToList()
                                        },
                                        Author = new User { Id = author.Id, FirstName = author.FirstName, LastName = author.LastName, RegisteredAt = author.RegisteredAt, Email = author.Email, Birthday = author.Birthday },
                                        Tasks = (from taskEl in tasks
                                                 join performer in allUsers on taskEl.PerformerId equals performer.Id
                                                 select new Models.Task
                                                 {
                                                     Id = taskEl.Id,
                                                     Name = taskEl.Name,
                                                     Description = taskEl.Description,
                                                     CreatedAt = taskEl.CreatedAt,
                                                     FinishedAt = taskEl.FinishedAt,
                                                     State = taskEl.State,
                                                     Performer = new User
                                                     {
                                                         Id = performer.Id,
                                                         FirstName = performer.FirstName,
                                                         Birthday = performer.Birthday,
                                                         RegisteredAt = performer.RegisteredAt,
                                                         Email = performer.Email,
                                                         LastName = performer.LastName
                                                     }
                                                 }).ToList()
                                    };
                foreach (var item in foundProjects)
                {
                    projects.Add(item);
                }

            }
            return projects;

        }
        private static List<TaskModel> GetAllTasks(HttpClient client)
        {
            var tasksResponse = client.GetAsync("https://bsa-dotnet.azurewebsites.net/api/" + "tasks").Result;
            return JsonConvert.DeserializeObject<List<TaskModel>>(tasksResponse.Content.ReadAsStringAsync().Result);
        }

        private static List<ProjectModel> GetAllProjects(HttpClient client)
        {
            var projectResponse = client.GetAsync("https://bsa-dotnet.azurewebsites.net/api/" + "projects").Result;
            return JsonConvert.DeserializeObject<List<ProjectModel>>(projectResponse.Content.ReadAsStringAsync().Result);
        }

        private static List<TeamModel> GetAllTeams(HttpClient client)
        {
            var teamResponse = client.GetAsync("https://bsa-dotnet.azurewebsites.net/api/" + "teams").Result;
            return JsonConvert.DeserializeObject<List<TeamModel>>(teamResponse.Content.ReadAsStringAsync().Result);
        }

        private static List<UserModel> GetAllUsers(HttpClient client)
        {
            var userResponse = client.GetAsync("https://bsa-dotnet.azurewebsites.net/api/" + "users").Result;
            return JsonConvert.DeserializeObject<List<UserModel>>(userResponse.Content.ReadAsStringAsync().Result);
        }
    }
}
