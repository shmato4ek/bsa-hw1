﻿using AutoMapper;
using HW1.BLL.ApiResultModels;
using HW1.BLL.Models;
using HW1.BLL.Structs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HW1.BLL.Services
{
    public class WebService
    {
        private List<Project> projects = new List<Project>();
        private int currentYear = 2021;
        private int maxLengthOfName = 45;
        private int minDescriptionLength = 20;
        private int maxAmmountOfTasks = 3;
        public WebService()
        {
            HttpService httpService = new HttpService();
            projects = httpService.GetRequest();
        }
        public Dictionary<Project, int> GetAmmountOfTasksOfConcreteUser(int userId)
        {
            var foundProjectsWithTasks = projects
                .Where(project => project.Author.Id == userId)
                .Select((project) => new
                {
                    Project = project,
                    AmmountOfTasks = project.Tasks.Count
                })
                .ToDictionary(d => d.Project, d => d.AmmountOfTasks);

            if (!foundProjectsWithTasks.Any())
            {
                throw new Exception("There is no project of this user or id is incorrect");
            }

            return foundProjectsWithTasks;
        }
        public List<Models.Task> GetAllTasksOfConcreteUser(int userId)
        {
            var tasks = projects
                .SelectMany(project => project.Tasks.Where(task => task.Performer.Id == userId && task.Name.Length < maxLengthOfName))
                .ToList();
            if (!tasks.Any())
            {
                throw new Exception("No tasks or ucorrect user id");
            }

            return tasks;
        }

        public Dictionary<int, string> GetFinishedTasksOfConcreteUser(int userId)
        {
            var foundTasks = projects
                .SelectMany(project => project.Tasks)
                .Where(task => task.FinishedAt != null && task.Performer.Id == userId && task.FinishedAt.Value.Year == currentYear)
                .Select(task => new
                {
                    Id = task.Id,
                    Name = task.Name
                })
                .ToDictionary(d => d.Id, d => d.Name);

            if (!foundTasks.Any())
            {
                throw new Exception("There is no tasks of this user or wrong id");
            }

            return foundTasks;
        }

        public List<TeamWithUsersStruct> GetTeamsWithUsersOlderThan10()
        {
            List<TeamWithUsersStruct> result = new List<TeamWithUsersStruct>();
            var foundTeams = projects
                .Select(project => project.Team)
                .Where(team => team.Users.All(user => user.Birthday.Year <= 2011))
                .GroupBy(team => team.Id)
                .Select(group => new
                {
                    Id = group.Key,
                    Users = group.Select(team => team.Users).Last().OrderByDescending(user => user.RegisteredAt),
                    Name = group.Select(team => team.Name).Last()
                });

            foreach (var item in foundTeams)
            {
                result.Add(new TeamWithUsersStruct { TeamName = item.Name, Id = item.Id, Users = item.Users});
            }
            return result;
        }

        public Dictionary<User, List<Models.Task>> GetSortedListOfUsers()
        {
            var foundTasks = projects
                .SelectMany(project => project.Tasks)
                .GroupBy(task => task.Performer.Id)
                .Select(group => new
                {
                    User = group.Select(task => task.Performer).Last(),
                    Tasks = group.OrderByDescending(task => task.Name).ToList()
                })
                .OrderBy(user => user.User.FirstName)
                .ToDictionary(d => d.User, d => d.Tasks);

            return foundTasks;
        }

        public UserStruct GetUserStruct(int userId)
        {
            UserStruct result = new UserStruct();

            var foundTasks = projects
                .Where(project => project.Author.Id == userId)
                .Select(project => project.Author)
                .GroupJoin(
                projects.SelectMany(project => project.Tasks),
                user => user.Id,
                task => task.Performer.Id,
                (user, tasks) => new
                {
                    User = user,
                    Tasks = tasks
                }
                )
                .GroupJoin(
                projects,
                user => user.User.Id,
                project => project.Author.Id,
                (user, projects) => new
                {
                    User = user.User,
                    Projects = projects,
                    Tasks = user.Tasks
                }
                ).Select(user => new
                {
                    LastProject = user.Projects.OrderBy(project => project.CreatedAt).Last(),
                    User = user.User,
                    AmmoutOfNotFinishedOrClosedTasks = user.Tasks.Where(task => task.FinishedAt == null || task.State == 0).Count(),
                    AmmountOfTaskInLastProject = user.Projects.OrderBy(project => project.CreatedAt).Last().Tasks.Count(),
                    TheLongestTask = user.Tasks.OrderBy(task => task.FinishedAt - task.CreatedAt).LastOrDefault()
                }).First();




            result.LastProject = foundTasks.LastProject;
            result.AmmoutOfNotFinishedOrClosedTasks = foundTasks.AmmoutOfNotFinishedOrClosedTasks;
            result.AmmountOfTaskInLastProject = foundTasks.AmmountOfTaskInLastProject;
            result.TheLongestTask = foundTasks.TheLongestTask;
            result.User = foundTasks.User;
            return result;
        }

        public List<ProjectStruct> GetProjectStruct()
        {
            List<ProjectStruct> result = new List<ProjectStruct>();
            var resultStruct = projects
                .Select(project => new
                {
                    Project = project,
                    TheShortestTask = project.Tasks.OrderBy(task => task.Name.Length).FirstOrDefault(),
                    TheLongestTask = project.Tasks.OrderBy(task => task.Description.Length).LastOrDefault(),
                    Ammount = (project.Description.Length > minDescriptionLength || project.Tasks.Count() < maxAmmountOfTasks) ? project.Team.Users.Count() : 0
                });
            foreach (var item in resultStruct)
            {
                result.Add(new ProjectStruct { ammountOfUsers = item.Ammount, Project = item.Project, theLongestTask = item.TheLongestTask, theShortestTask = item.TheShortestTask});
            }
            return result;
        }
    }
}
