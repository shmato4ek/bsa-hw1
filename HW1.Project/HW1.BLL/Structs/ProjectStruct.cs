﻿using HW1.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW1.BLL.Structs
{
    public struct ProjectStruct
    {
        public Project Project { get; set; }
        public Models.Task theLongestTask { get; set; }
        public Models.Task theShortestTask { get; set; }
        public int? ammountOfUsers { get; set; }
    }
}
