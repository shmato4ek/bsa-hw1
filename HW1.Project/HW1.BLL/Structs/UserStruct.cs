﻿using HW1.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW1.BLL.Structs
{
    public struct UserStruct
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int AmmountOfTaskInLastProject { get; set; }
        public int AmmoutOfNotFinishedOrClosedTasks { get; set; }
        public Models.Task TheLongestTask { get; set; }
    }
}
