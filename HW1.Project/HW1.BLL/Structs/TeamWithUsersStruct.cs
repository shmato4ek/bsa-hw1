﻿using HW1.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW1.BLL.Structs
{
    public struct TeamWithUsersStruct
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}
